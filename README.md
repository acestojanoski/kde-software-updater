# KDE Software Updater

## Description
Tool for updating software on Ubuntu Linux with KDE desktop environment

## Used technologies
* [electron](https://github.com/electron/electron)
* [electron-packager](https://github.com/electron-userland/electron-packager)
* [cross-env](https://github.com/kentcdodds/cross-env)
* [shelljs](https://github.com/shelljs/shelljs)

## Installation
1. Clone this repository: **git clone https://gitlab.com/acestojanoski/kde-software-updater.git**
2. Run **npm install** at root level
3. Run **npm build** (This will build the app in Linux x64 executable)
4. You can find the application in the folder KDESoftwareUpdater-linux-x64 at root level
5. The executable is KDESoftwareUpdater

## Creating desktop shortcut
1. In the **./home/{user}/.local/share/applications/** folder create a file with the name: **KDESoftwareUpdater.desktop**
2. Copy and modify this code in the file:

```
[Desktop Entry]
Encoding=UTF-8
Name=KDE Software Updater
Exec={your_path_to_the_packaged_application}
Icon={your_path_to_the_packaged_application}/resources/app/packages/icon.png
Terminal=false
Type=Application
Categories=Utility;
```

## Usage
* Update Lists (updates the package lists)
* Upgrade Software (upgrades the software that is out of date)
* Autoremove Software (autoremoves all unnecessary software packages)

![app main window](./images/main-window.png "Main Window")
