const url = require('url');
const path = require('path');
const about = require('./about');
const {app, BrowserWindow, Menu, ipcMain} = require('electron');

const shell = require('shelljs');
shell.config.execPath = '/usr/bin/node'; // Fix for the shelljs bug

let mainWindow;

if (process.env.NODE_ENV !== 'develop') {
    process.env.NODE_ENV = 'prod';
}

app.on('ready', () => {
    // Quit app if the platform is not linux
    if (process.platform !== 'linux') {
        app.quit();
    }

    // Window specifications
    mainWindow = new BrowserWindow({
        width: 500,
        height: 200,
        maxWidth: process.env.NODE_ENV !== 'prod' ? true : 500,
        maxHeight: process.env.NODE_ENV !== 'prod' ? true : 200,
        resizable: process.env.NODE_ENV !== 'prod' ? true : false,
        title: 'KDE Software Updater',
        icon: path.join(__dirname, 'packages', 'icon.png'),
        center: true,
        show: false,
    });

    mainWindow.once('ready-to-show', () => {
        mainWindow.show();
    });

    // Load main window html
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'src/htmls/main.html'),
        protocol: 'file:',
        slashes: true,
    }));

    // Close app completely when main window is closed
    mainWindow.on('closed', () => {
        app.quit();
    })

    // Load menu from template
    const mainMenu = Menu.buildFromTemplate(mainMenuTemplate);
    mainWindow.setMenu(mainMenu);
});

// Menu template
const mainMenuTemplate = [
    {
        label: 'File',
        submenu: [
            {
                label: 'About',
                click() {
                    about();
                }
            },
            {
                label: 'Quit',
                accelerator: process.platform === 'darwin' ? 'Command+Q' : 'Ctrl+Q',
                click() {
                    app.quit();
                }
            }
        ],
    },
];

// Button handlers
ipcMain.on('update', () => {
    shell.exec('konsole -e "sudo apt update"');
});

ipcMain.on('upgrade', () => {
    shell.exec('konsole -e "sudo apt upgrade"');
});

ipcMain.on('autoremove', () => {
    shell.exec('konsole -e "sudo apt autoremove"');
});

// Enable devTools if develop
if (process.env.NODE_ENV === 'develop') {
    mainMenuTemplate.push({
        label: 'DevTools',
        submenu: [
            {
                label: 'Toggle DevTools',
                accelerator: 'Ctrl+Shift+I',
                click(item, focusedWindow) {
                    focusedWindow.toggleDevTools();
                }
            }
        ],
    });
}
