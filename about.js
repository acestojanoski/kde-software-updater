const url = require('url');
const path = require('path');
const electron = require('electron');
const {BrowserWindow, Menu} = electron;

let aboutWindow;

const about = () => {
    aboutWindow = new BrowserWindow({
        width: 300,
        height: 200,
        maxWidth: 300,
        maxHeight: 200,
        resizable: false,
        title: 'About',
        icon: path.join(__dirname, 'packages', 'icon.png'),
        center: true,
        show: false,
    });

    aboutWindow.once('ready-to-show', () => {
        aboutWindow.show();
    });

    aboutWindow.setMenu(Menu.buildFromTemplate([]));

    aboutWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'src/htmls/about.html'),
        protocol: 'file:',
        slashes: true,
    }));

    aboutWindow.on('close', () => {
        aboutWindow = null;
    });
};

module.exports = about;
